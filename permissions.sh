#!/bin/bash

# Modify ownership
chown -R soundboard:www-data /var/www/soundboard
chown root:root /var/www/soundboard

# Set permissions
find "/var/www/soundboard" -type f -exec chmod 644 {} \;
find "/var/www/soundboard" -type d -exec chmod 755 {} \;
find "/var/www/soundboard/public_html" -type d -exec chmod 775 {} \;
find "/var/www/soundboard/public_html" -type f -exec chmod 664 {} \;
find "/var/www/soundboard/public_html/images" -type d -exec chmod 775 {} \;
find "/var/www/soundboard/public_html/images" -type f -exec chmod 664 {} \;
chmod 755 /var/www/soundboard/permissions.sh
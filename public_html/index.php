<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>sndbrd</title>
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <link rel="stylesheet" href="css/base.css">
  <link rel="stylesheet" href="css/skeleton.css">
  <link rel="stylesheet" href="css/layout.css">
  <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
    <!-- Favicons -->
  <link rel="shortcut icon" href="img/favicon.ico">
  <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
  <link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">
</head>
<body>

  <div class="container">
  <h1>SOUNDBOARD</h1>

  <ul id='sounds' class="count1 sixteen columns">
<?php
  function parse_dir($dir,$level){
    $dp=opendir($dir);
    $files=array();
    while (false!=($file=readdir($dp))){
      if ($file!="." && $file!=".."){
        array_push($files, $file);
        natsort($files);
      }
    }
    foreach($files as $key => $value){
      $name = explode('.', $value);
      $name = str_replace('_', ' ', $name[0]);
      echo "<li class=\"full-width button\">$name<audio preload=\"auto\"><source src=\"$dir$value\" type=\"audio/mp3\" /></audio></li>\n";
    }
  }
  /* ----------------- */
  parse_dir('mp3/',1);
?>
  </ul>
</div>

  <script src="js/libs/jquery-1.7.1.min.js"></script>
  <script src="js/plugins.js"></script>
  <script src="js/script.js"></script>
</body>
</html>